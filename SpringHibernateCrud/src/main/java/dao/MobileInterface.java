package dao;

import java.util.List;

import pojo.Mobile;

public interface MobileInterface {
	public void save(Mobile m);

	public List<Mobile> list();
}
