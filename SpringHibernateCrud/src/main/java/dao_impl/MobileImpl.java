package dao_impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import dao.MobileInterface;
import pojo.Mobile;

public class MobileImpl implements MobileInterface {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void save(Mobile m) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(m);
		session.flush();
		tx.commit();
		session.close();
	}

	public Mobile get(int id) {
		Session session = this.sessionFactory.openSession();
		Mobile m = (Mobile) session.get(Mobile.class, id);
		session.close();
		return m;
	}

	public Mobile load(int id) {
		Session session = this.sessionFactory.openSession();
		Mobile m = (Mobile) session.load(Mobile.class, id);
		session.close();
		return m;
	}

	public void delete(Mobile m) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.delete(m);
		session.flush();
		tx.commit();
		session.close();
	}

	public void update(Mobile m) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(m);
		session.flush();
		tx.commit();
		session.close();
	}

	public void merge(Mobile m) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.merge(m);
		session.flush();
		tx.commit();
		session.close();
	}

	public void saveOrUpdate(Mobile m) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(m);
		session.flush();
		tx.commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Mobile> list() {
		Session session = this.sessionFactory.openSession();
		List<Mobile> mobileList = session.createQuery("from Mobile").list();
		session.close();
		return mobileList;
	}

}
