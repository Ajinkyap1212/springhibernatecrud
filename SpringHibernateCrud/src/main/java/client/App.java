package client;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao_impl.MobileImpl;
import pojo.Mobile;
import pojo.Vendor;

/**
 * Hello world!
 *
 */
public class App {
	static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {
		DOMConfigurator.configure("src/main/java/resources/log4j.xml");
		logger.debug("Log4j appender configuration is successful !!");
		ApplicationContext context = new ClassPathXmlApplicationContext("resources/spring.xml");
		MobileImpl mobileImpl = (MobileImpl) context.getBean("mobile_impl");
		Vendor v1 = new Vendor("Samsung");
		Vendor v2 = new Vendor("Apple");
		Vendor v3 = new Vendor("Nokia");

		Mobile m1 = new Mobile("Android", v1);
		Mobile m2 = new Mobile("Ios", v2);
		Mobile m3 = new Mobile("Windows", v3);

		mobileImpl.save(m1);
		mobileImpl.save(m2);
		mobileImpl.save(m3);
		logger.trace("Mobile list is ready");

		List<Mobile> list = mobileImpl.list();

		for (Mobile mobile : list) {
			logger.trace(mobile + " : " + mobile.getVendor());
		}

		mobileImpl.delete(mobileImpl.get(3));
		logger.trace("After Delete");
		
		logger.trace("Mobeile List");
		List<Mobile> updatedList = mobileImpl.list();
		for (Mobile mobile : updatedList) {
			System.out.println(mobile + " : " + mobile.getVendor());
		}
		ClassPathXmlApplicationContext cpx = (ClassPathXmlApplicationContext) context;
		cpx.close();
	}
}
