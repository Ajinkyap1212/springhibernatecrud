package pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Vendor_info")
public class Vendor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vendor_id;
	private String vendor_name;

	public Vendor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vendor(String vendor_name) {
		super();
		this.vendor_name = vendor_name;
	}

	public String getVendor_name() {
		return vendor_name;
	}

	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}

	@Override
	public String toString() {
		return "\nVendor [vendor_id=" + vendor_id + ", vendor_name=" + vendor_name + "]";
	}

}
