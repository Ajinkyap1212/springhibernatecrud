package pojo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Mobile_info")
public class Mobile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mobile_id;
	private String mobile_os;

	@OneToOne(cascade = CascadeType.ALL)
	private Vendor vendor;

	public Mobile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mobile(String mobile_os, Vendor vendor) {
		super();
		this.mobile_os = mobile_os;
		this.vendor = vendor;
	}

	public String getMobile_os() {
		return mobile_os;
	}

	public void setMobile_os(String mobile_os) {
		this.mobile_os = mobile_os;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	@Override
	public String toString() {
		return "\nMobile [mobile_id=" + mobile_id + ", mobile_os=" + mobile_os + "]";
	}

}
